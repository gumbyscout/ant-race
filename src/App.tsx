import { useEffect, useMemo, useReducer, useState } from "react";
import cx from "classnames";

import { ReactComponent as AntSvg } from "./ant.svg";
import { ReactComponent as GrassSvg } from "./grass.svg";

function generateAntWinLikelihoodCalculator() {
  const delay = 7000 + Math.random() * 7000;
  const likelihoodOfAntWinning = Math.random();

  // I added the callback annotation but didn't change the implementation.
  // Hope that's OK.
  return (callback: (result: number) => void) => {
    setTimeout(() => {
      callback(likelihoodOfAntWinning);
    }, delay);
  };
}

interface Ant {
  name: string;
  length: number;
  color: "BLACK" | "RED" | "SILVER";
  weight: number;
}

type AntState = "not-asked" | "in-progress" | "calculated";

const AntCard = ({
  ant: { name, length, color, weight },
  uiState = "not-asked",
  odds = 0,
}: {
  ant: Ant;
  uiState: AntState;
  odds: number | undefined;
}) => (
  <div
    className={cx("border shadow-sm rounded-md overflow-hidden bg-green-100", {
      ["animate-pulse"]: uiState === "in-progress",
    })}
  >
    <div className="relative">
      {/* not sure why aspect is being kept, so had to manually tweak top */}
      <GrassSvg className="w-full absolute inset-0 -top-20 fill-green-400" />
      <AntSvg
        className={cx("w-full max-w-sm mx-auto p-4 relative", {
          ["fill-red-500"]: color === "RED",
          ["fill-black"]: color === "BLACK",
          ["fill-slate-500"]: color === "SILVER",
        })}
      />
    </div>
    <dl className="grid grid-cols-2 p-4 relative bg-green-400">
      <dt className="sr-only">name</dt>
      <dd className="text-xl font-bold col-span-2 truncate underline">
        {name}
      </dd>
      <dt className="font-bold">length</dt>
      <dd>{length}</dd>
      <dt className="font-bold">weight</dt>
      <dd>{weight}</dd>
      <dt className="font-bold">color</dt>
      <dd>{color}</dd>
      <dt className="font-bold">odds</dt>
      <dd>
        {" "}
        {uiState === "in-progress"
          ? "determining"
          : uiState === "calculated"
          ? `${(odds * 100).toFixed(0)}%`
          : "N/A"}
      </dd>
    </dl>
  </div>
);

const calculatedAntOddsAction = (antName: string, odds: number) => ({
  type: "calculated-odds",
  payload: { antName, odds },
});

const calculatingAntOdds = (antName: string) => ({
  type: "calculating-odds",
  payload: { antName },
});

const antOddsReducer = (
  state: { [antName: string]: { state: AntState; odds: number } } = {},
  action: { type: string; payload: any } // Didn't feel like re-remembering the correct way to generate ADTs. Wouldn't use `any` otherwise.
) => {
  switch (action.type) {
    case "calculated-odds":
      return {
        ...state,
        [action.payload.antName]: {
          odds: action.payload.odds,
          state: "calculated",
        },
      };
    case "calculating-odds":
      return {
        ...state,
        [action.payload.antName]: {
          ...state[action.payload.antName],
          state: "in-progress",
        },
      };
    default:
      return state;
  }
};

const AntsList = ({ ants }: { ants: Ant[] }) => {
  const [raceState, setRaceState] = useState<
    "not-started" | "calculating" | "completed"
  >("not-started");

  const [antOdds, dispatch] = useReducer(antOddsReducer, {});

  const mungedAnts = useMemo(
    () =>
      ants
        .map((ant) => ({
          ant,
          ...antOdds[ant.name],
        }))
        .sort((a, b) => {
          // sort inprogress after calcuated
          if (a.state === "in-progress" && b.state === "calculated") {
            return 1;
          }
          if (a.state === b.state) {
            // Sort by name if they are equal state
            if (a.state === "in-progress" || a.state === "not-asked")
              return a.ant.name.localeCompare(b.ant.name);

            // List is descending, bigger numbers first
            if (a.odds < b.odds) {
              return 1;
            }
          }
          return -1;
        }),
    [ants, antOdds]
  );

  const calculateRace = () => {
    setRaceState("calculating");
    Promise.all(
      ants.map((a) => {
        const calc = generateAntWinLikelihoodCalculator();
        dispatch(calculatingAntOdds(a.name));
        return new Promise<number>((res) => calc(res)).then((odds) =>
          dispatch(calculatedAntOddsAction(a.name, odds))
        );
      })
    ).then(() => setRaceState("completed"));
  };

  return (
    <>
      <p>
        Now the ants have been loaded, simply click "start race" or "race again"
        to determine the odds of an ant winning a race.
      </p>
      <button
        className="bg-teal-400 rounded-md shadow-sm p-4 uppercase disabled:opacity-50"
        onClick={calculateRace}
        disabled={raceState === "calculating"}
      >
        {raceState === "not-started"
          ? "Start race"
          : raceState == "calculating"
          ? "calculating"
          : "race again"}
      </button>
      <ul className="grid md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4 2xl:grid-cols-5 gap-4">
        {mungedAnts.map(({ ant, odds, state }) => (
          <li key={ant.name}>
            <AntCard ant={ant} odds={odds} uiState={state} />
          </li>
        ))}
      </ul>
    </>
  );
};

function App() {
  const [ants, setAnts] = useState<Ant[]>([]);
  const [uiState, setUIState] = useState<
    "initial" | "requested-ants" | "loading-ants" | "loaded-ants"
  >("initial");

  // Fetch our ants on request
  useEffect(() => {
    if (uiState == "requested-ants") {
      setUIState("loading-ants");
      fetch("/api/ants")
        .then((res) => res.json())
        .then((ants) => {
          setAnts(ants);
          setUIState("loaded-ants");
        });
    }
  }, [uiState]);

  return (
    <div className="container mx-auto space-y-4 px-4">
      <h1 className="text-3xl font-bold underline">Ant Race</h1>

      {uiState == "loaded-ants" ? (
        <AntsList ants={ants} />
      ) : (
        <>
          <p>
            Ant Race is a simple application that pits ants against each other
            in a randomized race. Click 'load data' below to get started!
          </p>
          <button
            className="bg-teal-400 rounded-md shadow-sm p-4 uppercase  disabled:opacity-50"
            onClick={() => setUIState("requested-ants")}
            disabled={uiState !== "initial"}
          >
            {uiState === "initial" ? "load data" : "loading..."}
          </button>
        </>
      )}
    </div>
  );
}

export default App;
