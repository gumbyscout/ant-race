import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

// The dummy server so we can make a fake api request.
import { createServer } from "miragejs";
const server = createServer({});
server.get("/api/ants", () => [
  { name: "Marie ‘Ant’oinette", length: 12, color: "BLACK", weight: 2 },
  { name: "Flamin’ Pincers", length: 11, color: "RED", weight: 2 },
  { name: "AuNT Sarathi", length: 20, color: "BLACK", weight: 5 },
  {
    name: "The Unbeareable Lightness of Being",
    length: 5,
    color: "SILVER",
    weight: 1,
  },
  { name: "‘The Duke’", length: 17, color: "RED", weight: 3 },
]);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
